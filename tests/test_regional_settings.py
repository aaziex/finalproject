import pytest

from utils.common_utils import is_value_expected


@pytest.mark.usefixtures("open_site")
class TestRegionalSettings:
    def test_change_currency_and_country(self, header):
        expected_country, expected_currency = header.update_regional_settings()
        actual_currency = header.get_current_currency_value()
        actual_country = header.get_current_country_value()
        is_actual_currency_valid = is_value_expected("currency", actual_currency, expected_currency)
        is_actual_country_valid = is_value_expected("country", actual_country, expected_country)

        assert is_actual_currency_valid, f"Currency on UI is different from expected after change. " \
                                         f"Expected {expected_currency}, actual {actual_currency}"

        assert is_actual_country_valid, f"Country on UI is different from expected after change. " \
                                        f"Expected {expected_country}, actual {actual_country}"
