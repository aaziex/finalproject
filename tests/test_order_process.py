import pytest

BASIC_USER_EMAIL = "new_user@mailforspam.com"
BASIC_USER_PASSWORD = "password123"


@pytest.mark.usefixtures("open_site")
class TestOrderProcess:
    def test_place_order(self, main_page, header, checkout_page, order_db_service, product_process_context):
        main_page.login(BASIC_USER_EMAIL, BASIC_USER_PASSWORD)
        header.open_catalog()
        expected_product_data = [
            product_process_context.add_random_product_to_cart(from_catalog=True),
            product_process_context.add_random_product_to_cart(product_quantity=2, from_similar=True)
        ]

        header.open_cart()
        actual_product_data = checkout_page.get_order_summary_data()
        is_order_summary_valid = checkout_page.is_order_summary_valid(expected_product_data, actual_product_data)
        assert is_order_summary_valid, f"Invalid order data, expected {expected_product_data} " \
                                       f"but got {actual_product_data}"

        order_time = checkout_page.confirm_order()
        result = order_db_service.is_order_created(BASIC_USER_EMAIL, order_time)
        assert result, f"Order created for {BASIC_USER_EMAIL} at {order_time} wasn't found at database"
