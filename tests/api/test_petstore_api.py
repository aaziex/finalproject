from utils.common_utils import is_value_expected


class TestPetStoreApi:
    def test_add_pet(self, pet_store_requests):
        add_new_pet_response = pet_store_requests.add_new_pet()
        pet_id = add_new_pet_response.json()["id"]
        get_pet_info_response = pet_store_requests.get_pet_info_by_id(pet_id)
        is_response_code_valid = is_value_expected("response code", get_pet_info_response.status_code, 200)

        assert is_response_code_valid, \
            f"Response code 200 was expected but got {get_pet_info_response.status_code}, " \
            f"pet wasn't found by newly created id={pet_id}, full response: {get_pet_info_response.json()}"

        actual_pet_id = get_pet_info_response.json()["id"]
        is_pet_id_valid = is_value_expected("pet_id", actual_pet_id, pet_id)

        assert is_pet_id_valid, f"Response contains info about wrong pet, " \
                                f"expected {actual_pet_id}, but got pet_id={pet_id}"

    def test_delete_pet(self, pet_store_requests):
        add_new_pet_response = pet_store_requests.add_new_pet()
        pet_id = add_new_pet_response.json()["id"]
        pet_store_requests.delete_pet(pet_id)
        response = pet_store_requests.get_pet_info_by_id(pet_id)
        is_response_code_valid = is_value_expected("response code", response.status_code, 404)

        assert is_response_code_valid, f"Pet wasn't deleted, response code 404 was expected but got " \
                                       f"{response.status_code}, full response: {response.json()}"
