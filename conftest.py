import allure
import pytest
from mysql.connector import connect
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

from api.petstore_requests import PetStoreRequests
from database.order_db_service import OrderDbService
from pages.catalog_page import CatalogPage
from pages.checkout_page import CheckoutPage
from pages.header import Header
from pages.main_page import MainPage
from pages.process_product_context import ProductProcessContext
from pages.product_page import ProductPage


@pytest.fixture
def open_site(main_page):
    with allure.step("Open main page"):
        main_page.navigate()


@pytest.fixture
def driver(request):
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--window-size=1920,1080")
    driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)

    yield driver

    test_name = request.node.name
    driver.save_screenshot(f"scr_{test_name}.png")
    allure.attach.file(f"scr_{test_name}.png", attachment_type=allure.attachment_type.PNG)
    driver.close()


# region Pages
@pytest.fixture
def main_page(driver):
    return MainPage(driver)


@pytest.fixture
def header(driver):
    return Header(driver)


@pytest.fixture
def catalog_page(driver):
    return CatalogPage(driver)


@pytest.fixture
def product_page(driver):
    return ProductPage(driver)


@pytest.fixture
def checkout_page(driver):
    return CheckoutPage(driver)
# endregion


@pytest.fixture
def product_process_context(header, product_page, catalog_page):
    return ProductProcessContext(header, product_page, catalog_page)


@pytest.fixture(scope="session")
def connection_to_litecart_db():
    connection = connect(
        host="127.0.0.1",
        user="root",
        password="",
        database="litecart"

    )

    yield connection
    connection.close()


@pytest.fixture
def order_db_service(connection_to_litecart_db):
    return OrderDbService(connection_to_litecart_db)


@pytest.fixture(scope="session")
def pet_store_requests():
    return PetStoreRequests()
