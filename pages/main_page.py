import allure

from pages.base_page import BasePage


class MainPage(BasePage):
    URL = "http://localhost/litecart/"
    EMAIL_INPUT_LOCATOR = "//input[@name='email']"
    PASSWORD_INPUT_LOCATOR = "//input[@name='password']"
    LOGIN_BUTTON_LOCATOR = "//button[@name='login']"

    def navigate(self):
        self.open_url(self.URL)

    def enter_email(self, username):
        self.clear_and_send_text(self.EMAIL_INPUT_LOCATOR, username)

    def enter_password(self, password):
        self.clear_and_send_text(self.PASSWORD_INPUT_LOCATOR, password)

    def click_login_button(self):
        self.click(self.LOGIN_BUTTON_LOCATOR)

    def login(self, username, password):
        with allure.step(f"Log in as {username}"):
            self.enter_email(username)
            self.enter_password(password)
            self.click_login_button()
