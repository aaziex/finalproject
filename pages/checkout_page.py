import re

import allure

from pages.base_page import BasePage
from utils.date_utils import get_current_datetime


class CheckoutPage(BasePage):
    CONFIRM_ORDER_BUTTON_LOCATOR = "//button[@name='confirm_order']"
    PRODUCT_NAME_LOCATOR = "//td[@class='item']"
    PRODUCT_TOTAL_LOCATOR = "//td[@class='sum']"

    def confirm_order(self):
        with allure.step("Confirm order"):
            self.click(self.CONFIRM_ORDER_BUTTON_LOCATOR)

            return get_current_datetime()

    def get_order_summary_data(self):
        names = self.get_elements_attributes(self.PRODUCT_NAME_LOCATOR, "innerHTML")
        prices = self.get_elements_attributes(self.PRODUCT_TOTAL_LOCATOR, "innerHTML")
        summary_data = []
        for name, price in zip(names, prices):
            summary_data.append(
                {
                    "product_name": name,
                    "total_cost": float(re.findall(r'\d*\.\d+|\d+', price)[0])
                }
            )

        return summary_data

    def is_order_summary_valid(self, expected_product_data, actual_product_data):
        with allure.step("Check order summary"):
            if len(actual_product_data) != len(expected_product_data):
                return False

            for product_data in actual_product_data:
                if product_data not in expected_product_data:
                    return False

            return True
