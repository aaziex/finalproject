import random

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    DEFAULT_TIME_OUT_SEC = 2

    def __init__(self, driver):
        self.driver = driver

    def _define_by_type(self, locator):
        if locator[0] == "/":
            return By.XPATH, locator

        return By.CSS_SELECTOR, locator

    def _find_element(self, locator, timeout=DEFAULT_TIME_OUT_SEC):
        locator_with_type = self._define_by_type(locator)

        return WebDriverWait(self.driver, timeout).until(ec.presence_of_element_located(locator_with_type))

    def _find_elements(self, locator, timeout=DEFAULT_TIME_OUT_SEC):
        locator_with_type = self._define_by_type(locator)

        return WebDriverWait(self.driver, timeout).until(ec.presence_of_all_elements_located(locator_with_type))

    def _get_random_element_from_list(self, locator):
        list_of_elements = self._find_elements(locator)

        return random.choice(list_of_elements)

    # region Clicks
    def click(self, locator):
        element = self._find_element(locator)
        element.click()

        return element

    def click_random_from_available(self, locator):
        element = self._get_random_element_from_list(locator)
        element.click()

        return element

    def click_and_get_random_element_attribute(self, locator, attribute):
        element = self.click_random_from_available(locator)

        return element.get_attribute(attribute)

    # endregion

    def get_element_attribute(self, locator, attribute):
        element = self._find_element(locator)

        return element.get_attribute(attribute)

    def get_elements_attributes(self, locator, attribute):
        elements_attributes = []
        for element in self._find_elements(locator):
            elements_attributes.append(element.get_attribute(attribute))

        return elements_attributes

    def open_url(self, url):
        self.driver.get(url)

    def clear_and_send_text(self, locator, text):
        element = self._find_element(locator)
        element.clear()
        element.send_keys(text)

    def is_element_present(self, locator):
        try:
            self._find_element(locator)
            return True
        except TimeoutException:
            return False

    def wait_element_attribute_changed(self, locator, text, attribute, timeout=DEFAULT_TIME_OUT_SEC):
        locator_with_type = self._define_by_type(locator)
        WebDriverWait(self.driver, timeout).until\
            (ec.text_to_be_present_in_element_attribute(locator_with_type, attribute, text))
