from pages.base_page import BasePage


class CatalogPage(BasePage):
    PRODUCT_LOCATOR = "//li[contains(@class,'product')]"

    def open_random_product_in_catalog(self):
        self.click_random_from_available(self.PRODUCT_LOCATOR)
