import re

from pages.base_page import BasePage


class ProductPage(BasePage):
    # region Locators
    ADD_TO_CART_LOCATOR = "//button[@name='add_cart_product']"
    SIMILAR_PRODUCT_LOCATOR = "//div[@id='box-similar-products']//li"
    QUANTITY_INPUT_LOCATOR = "//input[@name='quantity']"
    PRODUCT_SIZE_LOCATOR = "//select[contains(@name,'Size')]"
    PRODUCT_SIZE_VALUE_LOCATOR = "//option[@value='{}']"
    PRODUCT_PRICE_LOCATOR = "//div[@id='box-product']//span[@class='price']"
    CAMPAIGN_PRODUCT_PRICE_LOCATOR = "//div[@id='box-product']//strong[@class='campaign-price']"
    PRODUCT_NAME_LOCATOR = "//h1[@class='title']"
    # endregion

    def add_to_cart(self):
        is_product_has_size_select = self.is_element_present(self.PRODUCT_SIZE_LOCATOR)
        if is_product_has_size_select:
            self.select_product_size()
        self.click(self.ADD_TO_CART_LOCATOR)

    def open_random_similar_product(self):
        self.click_random_from_available(self.SIMILAR_PRODUCT_LOCATOR)

    def change_product_quantity(self, quantity):
        self.clear_and_send_text(self.QUANTITY_INPUT_LOCATOR, quantity)

    def select_product_size(self, size="Small"):
        self.click(self.PRODUCT_SIZE_LOCATOR)
        self.click(self.PRODUCT_SIZE_VALUE_LOCATOR.format(size))

    def get_product_name(self):
        return self.get_element_attribute(self.PRODUCT_NAME_LOCATOR, "innerHTML")

    def get_product_price(self):
        is_campaign_price_exist = self.is_element_present(self.CAMPAIGN_PRODUCT_PRICE_LOCATOR)
        if is_campaign_price_exist:
            product_price = self.get_element_attribute(self.CAMPAIGN_PRODUCT_PRICE_LOCATOR, "innerHTML")
        else:
            product_price = self.get_element_attribute(self.PRODUCT_PRICE_LOCATOR, "innerHTML")
        product_price = re.findall(r'\d*\.\d+|\d+', product_price)

        return float(product_price[0])

    def get_product_data(self):
        return self.get_product_name(), self.get_product_price()
