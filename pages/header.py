import allure

from pages.base_page import BasePage


class Header(BasePage):
    # region Locators
    CHANGE_SETTINGS_BUTTON_LOCATOR = "//a[@class='fancybox-region']"
    SAVE_SETTINGS_BUTTON_LOCATOR = "//button[@value='Save']"
    COMMON_NOT_SELECTED_ELEMENT_LOCATOR = "/child::option[not(@selected) and not(contains(text(),'Select'))]"

    CURRENCY_SELECTOR_LOCATOR = "//select[@name='currency_code']"
    NOT_SELECTED_CURRENCY_LOCATOR = CURRENCY_SELECTOR_LOCATOR + COMMON_NOT_SELECTED_ELEMENT_LOCATOR
    DISPLAYED_CURRENCY_LOCATOR = "//div[@class='currency']//span"

    COUNTRY_SELECTOR_LOCATOR = "//select[@name='country_code']"
    NOT_SELECTED_COUNTRY_LOCATOR = COUNTRY_SELECTOR_LOCATOR + COMMON_NOT_SELECTED_ELEMENT_LOCATOR
    DISPLAYED_COUNTRY_LOCATOR = "//div[@class='country']"

    CATALOG_BUTTON_LOCATOR = "//li[contains(@class,'category')]"

    CART_BUTTON_LOCATOR = "//a[contains(@href,'checkout') and @class='link']"
    CART_ITEMS_QUANTITY_LOCATOR = "//div[@id='cart']//span[@class='quantity']"
    # endregion

    def _open_regional_settings_modal(self):
        with allure.step("Open regional settings modal"):
            self.click(self.CHANGE_SETTINGS_BUTTON_LOCATOR)

    def _save_regional_settings(self):
        with allure.step("Save regional settings"):
            self.click(self.SAVE_SETTINGS_BUTTON_LOCATOR)

    def _open_currency_select(self):
        self.click(self.CURRENCY_SELECTOR_LOCATOR)

    def _open_country_select(self):
        self.click(self.COUNTRY_SELECTOR_LOCATOR)

    def update_regional_settings(self, change_country=True, change_currency=True):
        self._open_regional_settings_modal()
        changed_values = []

        if change_country:
            with allure.step("Select random country value"):
                self._open_country_select()
                country = self.click_and_get_random_element_attribute(self.NOT_SELECTED_COUNTRY_LOCATOR, "innerHTML")
                changed_values.append(country)

        if change_currency:
            with allure.step("Select random currency value"):
                self._open_currency_select()
                currency = self.click_and_get_random_element_attribute(self.NOT_SELECTED_CURRENCY_LOCATOR, "value")
                changed_values.append(currency)

        self._save_regional_settings()

        return changed_values

    def get_current_currency_value(self):
        return self.get_element_attribute(self.DISPLAYED_CURRENCY_LOCATOR, "innerHTML")

    def get_current_country_value(self):
        return self.get_element_attribute(self.DISPLAYED_COUNTRY_LOCATOR, "title")

    def open_catalog(self):
        self.click(self.CATALOG_BUTTON_LOCATOR)

    def open_cart(self):
        with allure.step("Open cart"):
            self.click(self.CART_BUTTON_LOCATOR)

    def get_number_of_products_in_cart(self):
        return self.get_element_attribute(self.CART_ITEMS_QUANTITY_LOCATOR, "innerHTML")

    def wait_product_added_to_cart(self, product_quantity=1):
        self.wait_element_attribute_changed(self.CART_ITEMS_QUANTITY_LOCATOR, str(product_quantity), "innerHTML")
