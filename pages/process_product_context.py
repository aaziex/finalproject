import allure


class ProductProcessContext:
    def __init__(self, header, product_page, catalog_page):
        self.header = header
        self.product_page = product_page
        self.catalog_page = catalog_page

    def add_random_product_to_cart(self, product_quantity=1, from_catalog=False, from_similar=False):
        with allure.step(f"Add {product_quantity} product(s) to cart"):
            if from_catalog:
                self.catalog_page.open_random_product_in_catalog()
            elif from_similar:
                self.product_page.open_random_similar_product()

            if product_quantity > 1:
                self.product_page.change_product_quantity(product_quantity)

            product_name, product_price = self.product_page.get_product_data()
            total_cost = product_price * product_quantity
            product_data = {"product_name": product_name,
                            "total_cost": total_cost}
            self.add_to_cart_and_wait_product_added(product_quantity)

            return product_data

    def add_to_cart_and_wait_product_added(self, product_quantity=1):
        products_in_cart = int(self.header.get_number_of_products_in_cart())
        expected_products_in_cart = products_in_cart + product_quantity
        self.product_page.add_to_cart()
        self.header.wait_product_added_to_cart(str(expected_products_in_cart))
