import allure

from database.base_sql_service import SqlService


class OrderDbService(SqlService):
    ORDER_ID_BY_DATE_QUERY = """
    SELECT id  
    FROM litecart.lc_orders
    WHERE customer_email = "{0}"
    AND date_created like "{1}%"
    """

    def is_order_created(self, user_email, order_datetime):
        with allure.step(f"Check order in database for {user_email} created at {order_datetime}"):
            query = self.ORDER_ID_BY_DATE_QUERY.format(user_email, order_datetime)
            result = self.execute_select_query(query)

            return len(result) > 0
