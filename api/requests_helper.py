import allure

from api.requests_data import RequestsData


class RequestsHelper:
    def get_add_pet_body(self, pet_name):
        body = RequestsData.ADD_PET_REQUEST_BODY
        body.update({"name": f"{pet_name}"})

        return body
