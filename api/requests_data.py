class RequestsData:
    ADD_PET_REQUEST_BODY = {
        "id": 0,
        "category": {
            "id": 0,
            "name": "string"
        },
        "name": "string",
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
                "id": 0,
                "name": "string"
            }
        ],
        "status": "available"
    }
