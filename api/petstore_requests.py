import allure
import requests

from api.requests_helper import RequestsHelper
from utils.string_utils import generate_random_string


class PetStoreRequests(RequestsHelper):
    BASE_URL = "https://petstore.swagger.io/v2"

    def _get(self, endpoint):
        return requests.get(self.BASE_URL + endpoint)

    def _post(self, endpoint, body):
        return requests.post(self.BASE_URL + endpoint, json=body)

    def _delete(self, endpoint):
        return requests.delete(self.BASE_URL + endpoint)

    def add_new_pet(self):
        random_name = generate_random_string()
        with allure.step(f"Add new pet via api (pet_name={random_name})"):
            body = self.get_add_pet_body(random_name)
            return self._post("/pet", body)

    def get_pet_info_by_id(self, pet_id):
        with allure.step(f"Get pet info via api (pet_id={pet_id})"):
            return self._get(f"/pet/{pet_id}")

    def delete_pet(self, pet_id):
        with allure.step(f"Delete pet via api (pet_id={pet_id})"):
            return self._delete(f"/pet/{pet_id}")
