import allure


def is_value_expected(parameter, actual_value, expected_value):
    with allure.step(f"Compare actual {parameter} value ({actual_value}) and expected ({expected_value})"):
        if actual_value == expected_value:
            return True

        return False
